import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  constructor(private http : HttpClient) { }
  getProduct(){
    return this.http.get<any>("http://localhost:8088/api/Employeeinfo")
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}