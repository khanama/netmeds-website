import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup ,FormBuilder,Validators} from '@angular/forms';
import { LoginserviceService } from 'src/app/services/loginservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  email:string="";
  password:string="";
  sampleData1:any=[];
  "loginForm": FormGroup;
  isSubmitted=false;
  constructor(private pdtservice: LoginserviceService, private route: Router,private formBuilder: FormBuilder) { }
  get(val:string,val1:string){
    this.email=val;
    this.password=val1;
  }
  nextpage(){
    this.pdtservice.getapi().subscribe((data: any)=>{
      this.sampleData1=data;
      for(let i=0;i<this.sampleData1.length;i++){
        if(this.email === this.sampleData1[i].email && this.password === this.sampleData1[i].password){
          console.warn("Signed in");
          this.route.navigate(["products"])
        }
      }
    })
  }
  ngOnInit() {
    this.loginForm=this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required]
    });
  }
  get formControls(){return this.loginForm.controls;}
  login(){
    console.log(this.loginForm.value);
    this.isSubmitted=true;
    if(this.loginForm.invalid){
      return;
    }
}
}
