import { Component, OnInit } from '@angular/core';
import { BrandService } from 'src/app/services/brand.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-admin-data',
  templateUrl: './admin-data.component.html',
  styleUrls: ['./admin-data.component.css']
})
export class AdminDataComponent implements OnInit {

  public productList : any;
  public filterCategory: any;
  searchKey:string ="";
  constructor(private apis: BrandService, private cartService: CartService) { }

  ngOnInit(): void {
    this.apis.getProduct()
    .subscribe(res=>{
      this.productList = res;
      this.filterCategory = res;

      this.productList.forEach((a: any)=>{
        if(a.category === "women's clothing" || a.category === "men's clothing"){
          a.category = "fashion"
        }
        Object.assign(a, {quantity:1, total:a.price});
      });
      console.log(this.productList);
    })
    this.cartService.search.subscribe((val: any)=>{
      this.searchKey = val;
    })
  }

  addtoCart(item: any){
    this.cartService.addtoCart(item);

  }

  filter(category: string){
    this.filterCategory = this.productList
    .filter((a: any)=>{
      if (a.category == category || category ==''){
        return a;
      }
    })
  }



}
