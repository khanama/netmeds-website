import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MedicinesComponent } from './components/medicines/medicines.component';
import { FilterPipe } from './services/filter.pipe';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminDataComponent } from './components/admin-data/admin-data.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MedicinesComponent,
    FilterPipe,
    CartComponent,
    LoginComponent,
    AdminDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
