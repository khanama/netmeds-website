import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDataComponent } from './components/admin-data/admin-data.component';
import { CartComponent } from './components/cart/cart.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { MedicinesComponent } from './components/medicines/medicines.component';

const routes: Routes = [
  {path:'', redirectTo:'products',pathMatch:'full'},
{path:'products', component: MedicinesComponent},
{path:'header', component: HeaderComponent},
{path:'cart', component: CartComponent},
{path:'login', component: LoginComponent},
{path:'adminData', component: AdminDataComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
