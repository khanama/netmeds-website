const express = require('express')

const router = express.Router()

require('./routes/employeeinfo')(router)

module.exports = router