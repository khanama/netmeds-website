const employeeinfo = require('../../models/employeeinfo')


module.exports=function(router){

//GET: the 12 newest stand-up mwwting notes
router.get('/employeeinfo', function (req, res){
//res.send("hello");
employeeinfo.find({}, (err, employeeinfo) => {
//check if error was found or not
if (err) {
res.json({ success: false, message:err }); //Return error message
}else{
// check if standup werefound in database
if (!employeeinfo) {
res.json({ success: false, message: 'No employeeinfo found.'}); // return error of no standup found
}
else{ 
res.json( employeeinfo ); //return success and stand array
}
}
})
})
//post:get new meeting note document...
router.post('/employeeinfo',function(req,res){
let note=new employeeinfo(req.body)
note.save(function(err,note){
if(err){
return res.status(400).json(err)
}
res.status(200).json(note)
})
})


router.put('/updateEmployeeinfo',(req,res)=>{
if(!req.body._id){
res.json({success:false,message:'No employee info id provided'});
}else{
employeeinfo.findOne({_id:req.body._id},(err,employeeinfo) => {
if(err){
res.json({ success:false,message:'Not a valid employee info id'})
}else{

employeeinfo.title=req.body.title;
employeeinfo.price=req.body.price;
employeeinfo.category=req.body.category;
employeeinfo.image=req.body.image;
employeeinfo.save((err)=>{
if(err){
res.json({success:false,message:err});
}else{
res.json({ success:true,message:'employeeinfo Updated!'});
}
});
}
});
}
});

router.delete('/deleteEmployeeinfo/:id',(req,res)=>{
if(!req.params.id){
res.json({success:false,message:'No is provided'});
}else{
employeeinfo.findOne({_id:req.params.id},(err,employeeinfo)=>{
if(err){
res.json({success:false,message:'Invalid id'});
}else{
employeeinfo.remove((err)=>{
if(err){
res.json({success:false,message:err});
}else{
res.json({success:true,message:'Employeeinfo deleted!'});
}
});
}
});
}
});



}



