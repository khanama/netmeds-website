const mongoose = require('mongoose')

const employeeinfoSchema = new mongoose.Schema({
  
  title:{type:String},
  price:{type:String},
  category:{type:String},
  image:{type:String}
})

module.exports = mongoose.model('Employeeinfo', employeeinfoSchema)