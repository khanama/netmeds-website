const mongoose=require('mongoose')
const productinfoSchema=new mongoose.Schema({
    id:{type:String},
    title:{type:String},
    price:{type:String},
    description:{type:String},
    category:{type:String},
    image:{type:String}
})
module.exports=mongoose.model('Productinfo',productinfoSchema)
